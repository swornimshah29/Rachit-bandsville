
import pandas as pd
from random import randint
import csv,json
import socket
import requests

#you can give any name in pd

# print('IMDB formula based filtering')
# metadata=pd.read_csv('./records.csv',low_memory=False)
# print(metadata.head(0))
# C = metadata['mRating'].mean()
# print(C)
# m = metadata['mFollowers'].quantile(0.90)
# print(m)
# #m is the minimum value which should be >=m for all other movies to be listed in the charts(recommendation)
# q_movies = metadata.copy().loc[(metadata['mFollowers'] >= m) & (metadata['mType']=='panist')]
# # q_movies = metadata.copy().loc[metadata['mFollowers'] >= m]

# #we have to give the weighted ranking (value) for each movies that were selected
# print(q_movies.shape)

# def weighted_ranking(x,m=m,C=C):
#     v = x['mFollowers']
#     R = x['mRating']
#     return (v/(v+m) * R) + (m/(m+v) * C)

# q_movies['score'] = q_movies.apply(weighted_ranking, axis=1)
# q_movies = q_movies.sort_values('score', ascending=False)
# #by default the result will be in the ascending order soo making it false

# print(len(q_movies))
# print(q_movies[['mId', 'mFollowers', 'mRating', 'score','mType','mLocation']].head(5))
# print('bands recommendation for the event organizer')


#chcp 65001 # comand: is the utf-8 encoding that is set on the windows console to avoid the charmap error

def processData(records):

    print('first object',records[0]['bId'])
    with open('./newBands.csv','w') as file:
        writer=csv.writer(file)
        #write each row
        writer.writerow(records[0].keys())#get the headers
        for eachBand in records:
            writer.writerow(eachBand.values())#prints the value as per the header

    metadata=pd.read_csv('./newBands.csv',low_memory=False)
    print(metadata.head(0))
    C = metadata['bRating'].mean()
    print(C)
    m = metadata['bFans'].quantile(0.5)#90th percentile
    print('percentile value ',m)
    #m is the minimum value which should be >=m for all other movies to be listed in the charts(recommendation)

    q_movies = metadata.copy().loc[metadata['bFans'] >= m]
    #we have to give the weighted ranking (value) for each movies that were selected
    print(q_movies)
    q_movies['bScore'] = q_movies.apply(weighted_ranking ,args=(m,C),axis=1)
    q_movies = q_movies.sort_values('bScore', ascending=False)
    #by default the result will be in the ascending order soo making it false
    print(q_movies)


def weighted_ranking(x,m,C):
    print('m',m)
    print('C',C)

    v = x['bFans']
    R = x['bRating']
    print('v',v)
    print('R',R)

    print('weighted ranking ',(v/(v+m) * R) + (m/(m+v) * C))
    return (v/(v+m) * R) + (m/(m+v) * C)


#send the recommendation updates for each bands
def sendToServer(bandsJson):
    # url = 'https://worldcupbetme.000webhostapp.com/bandsville/index.php'
    url = 'http://localhost/Rachit/bandsville-php/index.php'

    payload = {'POST_BANDS_NEW_RECOMMENDATION': bandsJson}
    headers = {'content-type': 'application/json'}
    r = requests.post(url, data=payload)
    print(r.status_code)
    print(r.text)

#get the bands and update the bands score or rating
def getFromServer():
    # url = 'https://worldcupbetme.000webhostapp.com/bandsville/index.php'
    url = 'http://localhost/Rachit/bandsville-php/index.php'

    payload = {'GET_BANDS': 'GET_BANDS'}
    headers = {'content-type': 'application/json'}
    r = requests.post(url, data=payload)
    print(r.status_code)
    print(r.text)
    records=json.loads(r.text)
    processData(records)



class Bands:
    bandsContainer=[]
    bandName=[]

    def __init__(self,bId,bName,bMembers,bRating,bLogo,bType,bFans):
        self.bId=bId
        self.bName=bName
        self.bMembers=bMembers
        self.bRating=bRating
        self.bLogo=bLogo
        self.bType=bType
        self.bFans=bFans

    @classmethod
    def addBands(cls,object):
        cls.bandsContainer.append(object)

    @classmethod
    def createJSON(cls):
        with open('./metal_bands_2017.csv','r') as file:
            reader=csv.DictReader(file)
            for row in reader:
                eachBand=Bands(
                    row['bandId'],
                    row['band_name'],
                    None,
                    randint(3,10),
                    None,
                    row['style'],
                    row['fans']
                )
                cls.bandsContainer.append(eachBand)

    @classmethod
    def createObject(cls,jsonString):
        JsonDIC=json.loads(jsonString)
        counter=len(json.loads(jsonString))#size of the jsonarray
        for eachJsonObject in JsonDIC:
            print(eachJsonObject['bName'])

        #send to the server

            
            

    # json_string=json.dumps([ob.__dict__ for ob in cls.bandsContainer])
    # print(type(json_string))
    # records=json.loads(json_string)
    # #records type list having dictionary as each item
    # print(type(json.loads(json_string)))
    # with open('./bands.csv','w') as file:
    #     writer=csv.writer(file)
    #     #write each row
    #     writer.writerow(records[0].keys())#get the headers
    #     for eachBand in records:
    #         writer.writerow(eachBand.values())#prints the value as per the header



# metadata=pd.read_csv('./bands.csv',low_memory=False)
# print(metadata.head(0))
# C = metadata['bRating'].mean()
# print(C)
# m = metadata['bFans'].quantile(0.90)#90th percentile
# print(m)
# #m is the minimum value which should be >=m for all other movies to be listed in the charts(recommendation)

# q_movies = metadata.copy().loc[metadata['bFans'] >= m]
# #we have to give the weighted ranking (value) for each movies that were selected
# print(q_movies.shape)

# def weighted_ranking(x,m=m,C=C):
#     v = x['bFans']
#     R = x['bRating']
#     return (v/(v+m) * R) + (m/(m+v) * C)

# q_movies['bScore'] = q_movies.apply(weighted_ranking, axis=1)
# q_movies = q_movies.sort_values('bScore', ascending=False)
# #by default the result will be in the ascending order soo making it false





# # print('top 15 bands are',q_movies)
# print('type ',type(q_movies))
# print(q_movies.to_json(orient='records'))
# json_data=q_movies.to_json(orient='records')
# print(len(json.loads(json_data)))
# Bands.createObject(json_data)
# print(type(json_data))
# sendToServer(json_data)

getFromServer()



# url = 'http://localhost/Rachit/bandsville-php/index.php'

# payload = {'GET_BANDS': 'GET_BANDS'}
# headers = {'content-type': 'application/json'}
# r = requests.post(url, data=payload)
# print(r.status_code)
# print(r.text)
# records=json.loads(r.text)






'''
Weighted formula details:
C=mean rating of all the bands in the database
R=average rating of each band in the database 
v=number of fans or followers for that band
m= percentile value in terms of followers or fans basis

'''



