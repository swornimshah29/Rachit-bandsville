import socket
import json

import pandas as pd
#you can give any name in pd

print('IMDB formula based filtering')
metadata=pd.read_csv('./records.csv',low_memory=False)
print(metadata.head(0))
C = metadata['mRating'].mean()
print(C)
m = metadata['mFollowers'].quantile(0.90)
print(m)
#m is the minimum value which should be >=m for all other movies to be listed in the charts(recommendation)
q_movies = metadata.copy().loc[(metadata['mFollowers'] >= m) & (metadata['mType']=='drummer')]
# q_movies = metadata.copy().loc[metadata['mFollowers'] >= m]

#we have to give the weighted ranking (value) for each movies that were selected
print(q_movies.shape)

def weighted_ranking(x,m=m,C=C):
    v = x['mFollowers']
    R = x['mRating']
    return (v/(v+m) * R) + (m/(m+v) * C)

q_movies['score'] = q_movies.apply(weighted_ranking, axis=1)
q_movies = q_movies.sort_values('score', ascending=False)
#by default the result will be in the ascending order soo making it false

print(type(q_movies))

HOST,PORT='',8888
listen_socket=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
listen_socket.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)
listen_socket.bind((HOST,PORT))
listen_socket.listen(1)
print('serving http server on port number ',PORT)
while True:
    client_connection,client_address=listen_socket.accept()
    request=client_connection.recv(2024)#get max bvyte size request
    print(request)
    print(q_movies[['mId', 'mFollowers', 'mRating', 'score','mType','mLocation']].head(5))
    client_connection.sendall(bytes(json.dumps([ob.__dict__ for ob in q_movies])
,'utf-8'))
    client_connection.close()
     