<?php

require_once('./Connection.php');
require_once('./Status.php');

//for both login and registration
class Authentication{



    var $uPNumber;
    var $uName;
    var $uType;
    var $uPassword;

    function alreadyRegistered($connection){
        
        $uPNumber=$this->uPNumber;
        $result=mysqli_query($connection,"SELECT * FROM registration where uPNumber='$uPNumber' ");
        $result=mysqli_fetch_assoc($result);
        if($result['uPNumber']==$uPNumber){
            return true;
        }else{
            return false;
        }

    }

    function makeRegistration($connection){
        
        $uPNumber=$this->uPNumber;
        $uName=$this->uName;
        $uType=$this->uType;
        $uPassword=$this->uPassword;
        
        $result=mysqli_query($connection,"INSERT INTO registration(uPNumber,uName,uType,uPassword) VALUES('$uPNumber','$uName','$uType','$uPassword') ");
        //also add to the musicians
        $bId=-1;
        $result=mysqli_query($connection,"INSERT INTO musicians(mId,mName,mType,bId) VALUES('$uPNumber','$uName','$uType','$bId') ");
        //-1 is the dummy id of the bandsTable for those who arenot associated with the bands yet
        //this is done because having foreign key in musicians wont allow you to insert rows withou the parent rows being first created
        
        if($result){
            return true;
        }else{
            return false;
        }
        
    }

    function makeLogin($connection){
        
        $uPNumber=$this->uPNumber;
        $uPassword=$this->uPassword;

        $result=mysqli_query($connection,"SELECT * FROM registration where uPNumber='$uPNumber' AND uPassword='$uPassword'  ");
        $result=mysqli_fetch_assoc($result);
        if($result['uPNumber']==$uPNumber && $result['uPassword']==$uPassword){
            return true;
        }else{
            return false;
        }
        
    }

}

if(isset($_POST['regCredentials'])){
    $connection=new Connection('localhost','root','','bandsville');
    $connection=$connection->connect();
    $regCredentials=json_decode($_POST['regCredentials'],true);
    $regObject=new Authentication;
    $statusObject=new Status;

    $regObject->uPNumber=$regCredentials['uPNumber'];
    $regObject->uName=$regCredentials['uName'];
    $regObject->uType=$regCredentials['uType'];
    $regObject->uPassword=$regCredentials['uPassword'];
    
    if($regObject->alreadyRegistered($connection)){
         $statusObject->statusCode='already';
         $statusObject->statusType='registration';
         echo json_encode($statusObject);
    }else{
        if($regObject->makeRegistration($connection)){
            $statusObject->statusCode='success';
            $statusObject->statusType='registration';
            echo json_encode($statusObject);
        }else{
            $statusObject->statusCode='failed';
            $statusObject->statusType='registration';
            echo json_encode($statusObject);
        }
        
    }

}


if(isset($_POST['loginCredentials'])){
    $connection=new Connection('localhost','root','','bandsville');
    $connection=$connection->connect();
    
        $loginCredentials=json_decode($_POST['loginCredentials'],true);
        $loginObject=new Authentication;
        $statusObject=new Status;
    
        $loginObject->uPNumber= $loginCredentials['uPNumber'];

        $loginObject->uPassword= $loginCredentials['uPassword'];
        
        if($loginObject->makeLogin($connection)){
             $statusObject->statusCode='success';
             $statusObject->statusType='login';
             echo json_encode($statusObject);
        }else{
            $statusObject->statusCode='failed';
            $statusObject->statusType='login';
            echo json_encode($statusObject);
        }
    
    }




?>