<?php
//deals with the notification like invitations and so on
require_once('./Connection.php');
require_once('./Status.php');

class Members{
    var $mName;
    var $mScore;
    var $mType;
}



class Notification{
    var $req_to;//mid of sender
    var $req_from;//mid of receiver
    var $req_to_name;//mName
    var $req_from_name;//mName
    var $sync_to;//synced by receiver
    var $sync_from;//synced by sender
    var $nId;//id of row of that table
    var $bId;
    var $bName;




    function requestToJoinBand($connection,$nCredentials){

        $statusObject=new Status;

        //check if already sent request
        $req_to=$nCredentials['req_to'];
        $req_from=$nCredentials['req_from'];
        $req_to_name=$nCredentials['req_to_name'];
        $req_from_name=$nCredentials['req_from_name'];
        $sync_to=$nCredentials['sync_to'];
        $sync_from=$nCredentials['sync_from'];
        $bId=$nCredentials['bId'];
        $bName=$nCredentials['bName'];
        

        //already sent request
        $alreadyQ=mysqli_query($connection,"SELECT COUNT(*) as count FROM notifications WHERE req_from='$req_from' AND req_to='$req_to' AND bId='$bId' ");
        $alreadyQ=mysqli_fetch_assoc($alreadyQ);
        if($alreadyQ['count']>0){
            $statusObject->statusCode='already';
            $statusObject->statusType='invite_request';
            echo json_encode($statusObject);
            return ;
        }
        //first time request sent
         $reqeustQ=mysqli_query($connection,
         "INSERT INTO `notifications` (req_from,req_to,req_from_name,req_to_name,sync_from,sync_to,bId,bName) 
         VALUES('$req_from','$req_to','$req_from_name','$req_to_name','$sync_from','$sync_to','$bId','$bName') ");

          if($reqeustQ){
            $statusObject->statusCode='success';
            $statusObject->statusType='invite_request';
            echo json_encode($statusObject);
          }else{
            $statusObject->statusCode='failed';
            $statusObject->statusType='invite_request';
            echo json_encode($statusObject);

          }

    }

    function getNotification($connection,$uPNumber){
        //get Combine notification
        //sync_to:true  means accepted
        //sync_to:false means rejected
        //sync_to:null means not seen by the receiver



        $notificationsList=NULL;

        //did someone sent me a request?
        $invite_request_notification=mysqli_query($connection,"SELECT * FROM notifications WHERE sync_to='null' AND req_to='$uPNumber' ");//sync_to null means i havent synced
        while($eachN=mysqli_fetch_assoc($invite_request_notification)){

            $nObject=new Notification;
            $nObject->req_from=$eachN['req_from'];
            $nObject->req_to=$eachN['req_to'];
            $nObject->req_from_name=$eachN['req_from_name'];
            $nObject->req_to_name=$eachN['req_to_name'];
            $nObject->sync_from=$eachN['sync_from'];
            $nObject->sync_to=$eachN['sync_to'];
            $nObject->bId=$eachN['bId'];
            $nObject->bName=$eachN['bName'];
            $nObject->nId=$eachN['nId'];//because when user accept or reject that row must be updated
            $notificationsList[]=$nObject;

        }

    
        //did my request got responded either accept or reject?
        $invite_response_notification=mysqli_query($connection,"SELECT * FROM notifications WHERE (req_from='$uPNumber' AND (sync_to='true' OR sync_to='false') AND (sync_from='null') ) ");
        //update instantly assuming user has seen the message
        
        while($eachN=mysqli_fetch_assoc($invite_response_notification)){

            $nObject=new Notification;
            $nObject->req_from=$eachN['req_from'];
            $nObject->req_to=$eachN['req_to'];
            $nObject->req_from_name=$eachN['req_from_name'];
            $nObject->req_to_name=$eachN['req_to_name'];
            $nObject->sync_from=$eachN['sync_from'];
            $nObject->sync_to=$eachN['sync_to'];
            $nObject->bId=$eachN['bId'];
            $nObject->bName=$eachN['bName'];
            $nObject->nId=$eachN['nId'];//because when user accept or reject that row must be updated
            $notificationsList[]=$nObject;

        }

        if($notificationsList!=NULL){
            echo json_encode($notificationsList);
        }else{
            $notificationsList=[];
            echo json_encode($notificationsList);
        }

    }

    //One whose invitation got responded and now that sender is making it seen
    function syncNotificationFromSender($connection,$nId){

        $syncQ=mysqli_query($connection,"UPDATE notifications SET sync_from='true' WHERE nId='$nId' ");
        if($syncQ){
            return true;
        }else{
            return false;
        }

    }

    //One who got invited for the band and now he/she is responding
    function syncNotificationFromReceiver($connection,$nId,$status){
    
        //on the basis of status make new bandMembers row, update the bID from -1 to user requested band
        // $syncQ=mysqli_query($connection,"UPDATE notifications SET sync_to='$status' WHERE nId='$nId' ");//original 
        
        $nQ=mysqli_query($connection,"SELECT * FROM notifications WHERE nId='$nId' ");
        if($nQ){
            $nQ=mysqli_fetch_assoc($nQ);
            $bId=$nQ['bId'];
            $mId=$nQ['req_to'];

            if(mysqli_query($connection,"UPDATE musicians SET bId='$bId' WHERE mId='$mId'")){
                $syncQ=mysqli_query($connection,"UPDATE notifications SET sync_to='$status' WHERE nId='$nId' ");
                return true;
            }else{
                return false;
            }
        
        }else{
            return false;
        }
        
    

        // if($syncQ){
        //     return true;
        // }else{
        //     return false;
        // }

    }
    
}



if(isset($_POST['requestToJoinBand']) && isset($_POST['notificationObject'])){
    $connection=new Connection('localhost','root','','bandsville');
    $connection=$connection->connect();

    $nCredentials=json_decode($_POST['notificationObject'],true);
    $nObject=new Notification;

    $nObject->requestToJoinBand($connection,$nCredentials);



}


if(isset($_POST['getNotification']) && isset($_POST['uPNumber'])){//uPNumber is also mId

    $connection=new Connection('localhost','root','','bandsville');
    $connection=$connection->connect();

    $nObject=new Notification;
    $nObject->getNotification($connection,$_POST['uPNumber']);
    
}


if(isset($_POST['syncNotificationFromReceiver']) && isset($_POST['nId']) && isset($_POST['status']) ){
    //update the nId of that uPNumber
    $connection=new Connection('localhost','root','','bandsville');
    $connection=$connection->connect();

    $nObject=new Notification;
    $statusObject=new Status;
    if($nObject->syncNotificationFromReceiver($connection,$_POST['nId'],$_POST['status'])){
        $statusObject->statusType='syncNotification';
        $statusObject->statusCode='success';
        echo json_encode($statusObject);
    }else{
        $statusObject->statusType='syncNotification';
        $statusObject->statusCode='failed';
        echo json_encode($statusObject);
    }
    
}


if(isset($_POST['syncNotificationFromSender']) && isset($_POST['nId']) ){
    //update the nId of that uPNumber
    $connection=new Connection('localhost','root','','bandsville');
    $connection=$connection->connect();

    $nObject=new Notification;
    $statusObject=new Status;
    if($nObject->syncNotificationFromSender($connection,$_POST['nId'])){
        $statusObject->statusType='syncNotification';
        $statusObject->statusCode='success';
        echo json_encode($statusObject);
    }else{
        $statusObject->statusType='syncNotification';
        $statusObject->statusCode='failed';
        echo json_encode($statusObject);
    }
    
}




if(isset($_POST['getBandsInformation']) && isset($_POST['uPNumber'])){
    $uPNumber=$_POST['uPNumber'];
    $result=mysqli_query($connection,"SELECT * FROM musicians WHERE mId='$uPNumber' ");
    $result=mysqli_fetch_assoc($result);
    $bId=$result['bId'];
    $membersList=NULL;

    $result=mysqli_query($connection,"SELECT * FROM musicians WHERE bId='$bId' ");
    while($each=mysqli_query($result)){
        $eachM=new Members();
        $eachM->mName=$each['mName'];
        $eachM->mType=$each['mType'];
        $eachM->mScore=$each['mScore'];
        $membersList[]=$eachM;   
    }

    if($membersList==NULL){
        $membersList=[];
        echo json_encode($membersList);
    }else {
        echo json_encode($membersList);
    }

}
    
    
    






?>