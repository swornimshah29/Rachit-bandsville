<?php

require_once('./Connection.php');
require_once('./Status.php');


function customLocationSorting($band1,$band2){
    
    if($band1->distance<$band2->distance){return -1;}//if band1(always first parameter is focused) is smaller
    if($band1->distance>$band2->distance){return 1;}//if band1(always first parameter is focused) is smaller
    return 0;
}



//for bands related operation
class Bands{

    //this represents the client class structure for bands(android java class)
    var $bId;
    var $bName;
    var $bMembers;
    var $bType;//every bandType has a number to represent rather than a words maybe i dont know need to do it
    var $bLogo;
    var $bFans;
    var $bRating;
    var $bLocationLat;
    var $bLocationLon;
    var $distance;//for comparison between locations


    
    function rateBands($connection,$bId,$rating,$ratedBy){
        
        if($result=mysqli_query($connection,"SELECT COUNT(*) AS numRates FROM bandsRating WHERE ratedBy='$ratedBy' AND bId='$bId' ")){
            $result=mysqli_fetch_assoc($result);
            if($result['numRates']>0){
                //duplicates rating
                //update the ratings
                if(mysqli_query($connection,"UPDATE bandsRating SET bScore='$rating' WHERE bId='$bId' AND ratedBy='$ratedBy' ")){
                    return true;
                }else{
                    return false;                    
                }
            }else{

                if(mysqli_query($connection,"INSERT INTO `bandsRating` (bId,bScore,ratedBy) VALUES('$bId','$rating','$ratedBy') ")){
                    return true;
                }else{
                    return false;
                }

            }
        }else{
            return false;
        }  
    }


    function followBands($connection,$followedBy,$followedTo){
        //insert into mFollowers then update the musicians followers
        
        if($result=mysqli_query($connection,"SELECT COUNT(*) AS totalFollowers FROM bFollowers WHERE followedTo='$followedTo' AND followedBy='$followedBy' ")){//avoid duplicates
            $result=mysqli_fetch_assoc($result);
            $totalFollowers=$result['totalFollowers'];
            if($totalFollowers>0){
                return false;//already followed by him
            }else{
                //insert new data
            if(mysqli_query($connection,"INSERT INTO bFollowers(followedTo,followedBy) VALUES('$followedTo','$followedBy') ")){
                $result=mysqli_query($connection,"SELECT COUNT(*) AS totalFollowers FROM bFollowers WHERE followedTo='$followedTo' ");
                $result=mysqli_fetch_assoc($result);
                $totalFollowers=$result['totalFollowers'];
            
            // echo $totalFollowers;
            if(mysqli_query($connection,"UPDATE bands SET bFans='$totalFollowers' WHERE bId='$followedTo' ")){
                return true;
            }else{
                return false;
            }

        }else{
            return false;
        }
    
        }

    }else{
        return false;
    }
}


    //form a new band in the database
    function makeNewBand($bCreatedBy){
        $connection=new Connection('localhost','root','','bandsville');
        $connection=$connection->connect();
        
        $bName=$this->bName;
        $bType=$this->bType;
        $bLogo= urlencode($this->bLogo);
        $bLocationLat=$this->bLocationLat;
        $bLocationLon=$this->bLocationLon;
        


        //delete the old band and make new insert
        $bId=NULL;

        $selectQ=mysqli_query($connection,"SELECT * FROM bands WHERE bcreatedBy='$bCreatedBy' ");
        if($selectQ){

            while($each=mysqli_fetch_assoc($selectQ)){
                $bId=$each['bId'];
            }
        }

        if($bId!=NULL){
            mysqli_query($connection,"UPDATE bands SET bStatus='invalid' WHERE bId='$bId' ");
            
            $result=mysqli_query($connection,
            "INSERT INTO bands (bName,bType,bLogo,bCreatedBy,bLocationLat,bLocationLon) VALUES('$bName','$bType','$bLogo','$bCreatedBy','$bLocationLat','$bLocationLon') ");

            if($result){
                $bId=mysqli_insert_id($connection);
               //update -1 dummy bId with newbId
                mysqli_query($connection,"UPDATE musicians SET bId='$bId' WHERE mId='$bCreatedBy' ");
                return [true,$bId];
            }else{
                return [false,'none'];//this makes the users still say you havent associated to any bands
            }
        }else{
            $result=mysqli_query($connection,
            "INSERT INTO bands (bName,bType,bLogo,bCreatedBy,bLocationLat,bLocationLon) VALUES('$bName','$bType','$bLogo','$bCreatedBy','$bLocationLat','$bLocationLon') ");

            if($result){
                $bId=mysqli_insert_id($connection);
                //update -1 dummy bId with newbId
                mysqli_query($connection,"UPDATE musicians SET bId='$bId' WHERE mId='$bCreatedBy' ");
                
                return [true,$bId];
            }else{
                return [false,'none'];//this makes the users still say you havent associated to any bands
            }
        }
       
    }

    /*-------------------------------------------Recommendation filtereing on the basis of location, bTypes and bScore or bRating----------------------------------------------- */
  
    function getRecommendedBands($connection){
        
        $tempRecommendedBandList=[];//without distance value
        $recommendedBandList=[];//with distance value
        

        $bRating=$this->bRating;
        $bType=$this->bType;
        $bId=$this->bId;//dont recommend my own band
        
        $lat1=$this->bLocationLat;
        $lon1=$this->bLocationLon;
                
        //get other bands which is not me (bId!='$bId') also dont get my invalid band(bStatus='valid')
        $bandsQ=mysqli_query($connection,"SELECT * FROM bands where bType='$bType' AND bId!='$bId' AND bStatus='valid' ");
        // $bandsQ=mysqli_query($connection,"SELECT * FROM bands where bRating>='$lowerLimit' AND bRating<='$upperLimit' AND bType='Rock' AND bId!='$bId' ");
    
        while($each=mysqli_fetch_assoc($bandsQ)){

            $eachBand=new Bands;
            $eachBand->bLocationLat=$each['bLocationLat'];
            $eachBand->bLocationLon=$each['bLocationLon'];
            $eachBand->bName=$each['bName'];
            $eachBand->bType=$each['bType'];
            $eachBand->bRating=$each['bRating'];
            $eachBand->bLogo=$each['bLogo'];
            $eachBand->bMembers=$each['bMembers'];
            $eachBand->bId=$each['bId'];
            $eachBand->bFans=$each['bFans'];
            
            $tempRecommendedBandList[]=$eachBand;
            
            // echo 'bands are'.$each['bName'].'</br>';
            
        }

        //find the nearby bands
        foreach($tempRecommendedBandList as $each){
         
            $lon2=$each->bLocationLon;
            $lat2=$each->bLocationLat;
            $theta = $lon1 - $lon2;//lon2
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;                
            $each->distance=$miles;//update the distance of that bandObject
            $recommendedBandList[]=$each;
        }
        //filter the recommendedBandList and get the top 10 bands nearby you
        //-1 (smaller than), 0 (equal to), or 1 (larger than)
        usort($recommendedBandList,"customLocationSorting");
        //recommendedBandList has been sorted

        echo json_encode($recommendedBandList);
        
    }
    


}

//uPNumber is for who created the band
if(isset($_POST['makeNewBand']) && isset($_POST['uPNumber'])){

    $newBand=json_decode($_POST['makeNewBand'],true);
    $bandObject=new Bands;
    $statusObject=new Status;
    $bandObject->bName=$newBand['bName'];
    $bandObject->bType=$newBand['bType'];
    $bandObject->bLogo=$newBand['bLogo']; 

    $bandObject->bLocationLat=$newBand['bLocationLat'];
    $bandObject->bLocationLon=$newBand['bLocationLon'];

    $bCreatedBy=$_POST['uPNumber'];
    $response=$bandObject->makeNewBand($bCreatedBy);
    if($response[0]){
        $statusObject->statusCode='success';
        $statusObject->statusType=$response[1];//bId
        echo json_encode($statusObject);
    }else{
        $statusObject->statusCode='failed';
        $statusObject->statusType='makeNewBand';
        echo json_encode($statusObject);
    }

    

}



//rate musicians
if(isset($_POST['rateBands']) && isset($_POST['ratedScore']) && isset($_POST['bId']) && isset($_POST['ratedBy'])){
    $connection=new Connection('localhost','root','','bandsville');
    $connection=$connection->connect();

    $bId=$_POST['bId'];
    $ratedScore=$_POST['ratedScore'];
    $ratedBy=$_POST['ratedBy'];

    $statusObject=new Status;
    $bObject=new Bands;
    if($bObject->rateBands($connection,$bId,$ratedScore,$ratedBy)){
        $statusObject->statusCode='success';
        $statusObject->statusType='rating';
        echo json_encode($statusObject);
    }else{
        $statusObject->statusCode='failed';
        $statusObject->statusType='rating';
        echo json_encode($statusObject);
    }
    

}

if(isset($_POST['followBands']) && isset($_POST['followedBy']) && isset($_POST['followedTo'])){
    $connection=new Connection('localhost','root','','bandsville');
    $connection=$connection->connect();

    $followedBy=$_POST['followedBy'];//mid
    $followedTo=$_POST['followedTo'];//bId
    
    $statusObject=new Status;
    $bObject=new Bands;
    if($bObject->followBands($connection,$followedBy,$followedTo)){
        $statusObject->statusCode='success';
        $statusObject->statusType='following';
        echo json_encode($statusObject);
    }else{
        $statusObject->statusCode='failed';
        $statusObject->statusType='following';
        echo json_encode($statusObject);
    }
    

}


//get recommended bands
if(isset($_POST['getRecommendedBands']) && isset($_POST['uPNumber'])){
    
    $connection=new Connection('localhost','root','','bandsville');
    $connection=$connection->connect();  

    $uPNumber=$_POST['uPNumber'];
    $bandObject=new Bands;

    //get the user number and get his/her associated bandId
    $myBandQuery=mysqli_query($connection,
    "SELECT B.bType,B.bFans,B.bId,B.bRating,B.bLocationLat,B.bLocationLon,B.bStatus,M.mId FROM bands as B INNER JOIN musicians as M ON M.bId = B.bId  WHERE M.mId='$uPNumber' AND B.bStatus='valid' ");

    if($myBandQuery){
        $result=mysqli_fetch_assoc($myBandQuery);
        $bandObject->bLocationLat=$result['bLocationLat'];
        $bandObject->bLocationLon=$result['bLocationLon']; 
        $bandObject->bType=$result['bType'];
        $bandObject->bRating=$result['bRating'];
        $bandObject->bId=$result['bId'];
        $bandObject->bFans=$result['bFans'];
        $bandObject->mId=$result['mId'];
        
        $bandObject->getRecommendedBands($connection);
        //bId is the band that the user is connected to
        //this information is hold in the musicians table 
    }

        
    }



   //lat1 and lon1 are the user send locations
    //lat2 and lon2 are the comparisions locations
    function distance($lat1, $lon1, $lat2, $lon2, $unit) {
        
          $theta = $lon1 - $lon2;//each.lon2
          $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
          $dist = acos($dist);
          $dist = rad2deg($dist);
          $miles = $dist * 60 * 1.1515;
          $unit = strtoupper($unit);
        
          if ($unit == "K") {
              return ($miles * 1.609344);
          } else if ($unit == "N") {
              return ($miles * 0.8684);
          } else {
              return $miles;
          }
          
        }


        function rateAll(){

            $connection=new Connection('localhost','root','','bandsville');
            $connection=$connection->connect();  
        
            $result=mysqli_query($connection,
            "SELECT B.bFans,B.bId,AVG(R.bScore) as bRating FROM bands as B INNER JOIN bandsrating as R ON R.bId = B.bId GROUP BY bId");
            if($result){
                while($each=mysqli_fetch_assoc($result)){
                    $bandObj=new Bands;
                    $bandObj->bId=$each['bId'];
                    $bandObj->bRating=$each['bRating'];
                    $bandObj->bFans=$each['bFans'];
                    $bandsList[]=$bandObj;
                    //send json as {bId,bRating}
                    
                }

                echo json_encode($bandsList);
                foreach($bandsList as $each){
                    $bId=$each->bId;
                    $bRating=$each->bRating;
                    
                    mysqli_query($connection,"UPDATE bands SET bRating='$bRating' WHERE bId='$bId'  ");
                }
        
        }


    }


   // rateAll();

   function get_percentile($percentile, $array) {
    sort($array);
    $index = ($percentile/100) * count($array);
    if (floor($index) == $index) {
         $result = ($array[$index-1] + $array[$index])/2;
    }
    else {
        $result = $array[floor($index)];
    }
    return $result;
}


function getrecommend(){
    $connection=new Connection('localhost','root','','bandsville');
    $connection=$connection->connect();  

    $result=mysqli_query($connection,
    "SELECT B.bFans,B.bId,AVG(R.bScore) as bRating FROM bands as B INNER JOIN bandsrating as R ON R.bId = B.bId GROUP BY bId");
    // "SELECT *,AVG(newResult.bRating) AS C FROM ( SELECT B.bFans,B.bId,AVG(R.bScore) as bRating FROM bands as B INNER JOIN bandsrating as R ON R.bId = B.bId GROUP BY bId)newResult";
    
    $bandsList=NULL;
    $followersList=NULL;
    
    if($result){
        while($each=mysqli_fetch_assoc($result)){
            $bandObj=new Bands;
            $bandObj->bId=$each['bId'];
            $bandObj->bRating=$each['bRating'];//R
            $bandObj->bFans=$each['bFans'];//V
            $followersList[]=$each['bFans'];
            $bandsList[]=$bandObj;
            //send json as {bId,bRating}
        }

        if($bandsList!=NULL){
            //mean rating of all musicians/bands as C
            $CQuery="SELECT AVG(newResult.bRating) as C FROM 
            ( SELECT B.bFans,B.bId,AVG(R.bScore) as bRating FROM bands as B INNER JOIN bandsrating as R ON R.bId = B.bId GROUP BY bId)newResult";
            $result=mysqli_query($connection,$CQuery);
            $result=mysqli_fetch_assoc($result);
            $C=$result['C'];

            //create array of followers and sent to percentile formula to get the percentile score as M
           $M= get_percentile(75,$followersList);
           echo json_encode($followersList);
            
            
        }else{
            $bandsList=[];
            echo json_encode($bandsList);

        }

     

    }

}


//This is the recommendation algorithm for the bands 
function updateNewBandScore($connection){

    echo 'starting the recommendation system of all the bands score updates';

    /*Dont worry about invalid bands because client never sees them in the android (filtered during get recommendation bands) */
    $result=mysqli_query($connection,
    "SELECT B.bName,B.bFans,B.bId,AVG(R.bScore) as bRating FROM bands as B INNER JOIN bandsrating as R ON R.bId = B.bId GROUP BY bId");
    // "SELECT *,AVG(newResult.bRating) AS C FROM ( SELECT B.bFans,B.bId,AVG(R.bScore) as bRating FROM bands as B INNER JOIN bandsrating as R ON R.bId = B.bId GROUP BY bId)newResult";
    
    $bandsList=NULL;
    $followersList=NULL;
    
    if($result){
        while($each=mysqli_fetch_assoc($result)){
            $bandObj=new Bands;
            $bandObj->bId=$each['bId'];
            $bandObj->bRating=$each['bRating'];//R
            $bandObj->bFans=$each['bFans'];// for musicians it is mFollowers
            $bandObj->bName=$each['bName'];
            
            $followersList[]=$each['bFans'];
            $bandsList[]=$bandObj;
            //send json as {bId,bRating}
        }

        if($bandsList!=NULL){
            //mean rating of all musicians/bands as C
            $CQuery="SELECT AVG(newResult.bRating) as C FROM 
            ( SELECT B.bFans,B.bId,AVG(R.bScore) as bRating FROM bands as B INNER JOIN bandsrating as R ON R.bId = B.bId GROUP BY B.bId)newResult";
            $result=mysqli_query($connection,$CQuery);
            $result=mysqli_fetch_assoc($result);
            $C=$result['C'];//C 

            //create array of followers and sent to percentile formula to get the percentile score as M
           $M= get_percentile(75,$followersList);//M

           //update newWeighted Score for each rows in the bands table by the weighted formula
           foreach($bandsList as $each){
               $bId=$each->bId;
               $V=$each->bFans;
               $R=$each->bRating;
               if($V+$M==0){
               }else{
                $weightedScore=( (($V*$R)/($V+$M)) +(($M*$C)/($M+$V)) );
                echo 'new score of '.$each->bName.''.' is '.$weightedScore."</br>"; 
                //update the row
                mysqli_query($connection,"UPDATE bands SET bRating='$weightedScore' WHERE bId='$bId' ");
            }

            }
           
           }
            
            
        }else{
            echo 'No bands found on the bandsville server';

        }

    }



// $connection=new Connection('localhost','root','','bandsville');
// $connection=$connection->connect();
// updateNewBandScore($connection);
?>
