<?php

require_once('./Connection.php');
require_once('./Status.php');

//for bands related operation


function get_percentile($percentile, $array) {
    sort($array);
    $index = ($percentile/100) * count($array);
    if (floor($index) == $index) {
         $result = ($array[$index-1] + $array[$index])/2;
    }
    else {
        $result = $array[floor($index)];
    }
    return $result;
}


function customLocationSortingByLocation($musician1,$musician2){
    //ascending sort
    if($musician1->distance<$musician2->distance){return -1;}//if band1(always first parameter is focused) is smaller
    if($musician1->distance>$musician2->distance){return 1;}//if band1(always first parameter is focused) is smaller
    return 0;
}

function customLocationSortingByScore($musician1,$musician2){
    
    //descending order
    if($musician1->mScore<$musician2->mScore){return 1;}//if band1(always first parameter is focused) is smaller
    if($musician1->mScore>$musician2->mScore){return -1;}//if band1(always first parameter is focused) is smaller
    return 0;
}



class Musicians{

    //this represents the client class structure for bands
    var $mId;
    var $mName;
    var $mType;
    var $mProfileP;
    var $mScore;
    var $mLocationLat;
    var $mLocationLon;
    var $bName;//associated band Name
    var $distance;
    var $mFollowers;

    //sends all the recommended musicians to the user
    function syncUserPhoto($connection,$mId,$mProfileP){
        $mProfileP= urlencode($mProfileP);
        
        $updateQ=mysqli_query($connection,"UPDATE musicians SET mProfileP='$mProfileP' WHERE mId='$mId' ");
        if($updateQ){
            return true;
        }
        return false;
    }

    function getMusiciansRecommendationByScore($connection,$mCredentials){
        
        $tempRecommendedMusiciansList=[];//without distance value
        $recommendedMusiciansList=[];//with distance value
            
        $lat1=$mCredentials['mLocationLat'];
        $lon1=$mCredentials['mLocationLon'];
        $mId=$mCredentials['mId'];//dont recommend me
        
    
        $musiciansQ=mysqli_query($connection,
        "SELECT M.mId,M.mName,M.mFollowers,M.mType,M.mLocationLat,M.mLocationLon,M.mProfileP,M.mScore,B.bName 
        FROM musicians as M INNER JOIN bands as B ON M.bId= B.bId WHERE M.mId!='$mId'");
    
        while($each=mysqli_fetch_assoc($musiciansQ)){
    
            $eachMusician=new Musicians;
            $eachMusician->mLocationLat=$each['mLocationLat'];
            $eachMusician->mLocationLon=$each['mLocationLon'];
            $eachMusician->mName=$each['mName'];
            $eachMusician->mType=$each['mType'];
            $eachMusician->mScore=$each['mScore'];
            $eachMusician->mProfileP=$each['mProfileP'];
            $eachMusician->mId=$each['mId'];
            $eachMusician->mFollowers=$each['mFollowers'];
            
            $eachMusician->bName=$each['bName'];
            
            $tempRecommendedMusiciansList[]=$eachMusician;
            
            // echo 'bands are'.$each['bName'].'</br>';
            
        }
    
        //find the nearby bands
        foreach($tempRecommendedMusiciansList as $each){
         
            $lon2=$each->mLocationLon;
            $lat2=$each->mLocationLat;
            $theta = $lon1 - $lon2;//lon2
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;                
            $each->distance=$miles;//update the distance of that bandObject
            $recommendedMusiciansList[]=$each;
        }
        //filter the recommendedBandList and get the top 10 bands nearby you
        //-1 (smaller than), 0 (equal to), or 1 (larger than)
        usort($recommendedMusiciansList,"customLocationSortingByScore");//just sort them on the basis of score
        
        //recommendedBandList has been sorted
    
        echo json_encode($recommendedMusiciansList);
        
        }
    
    
        function rateMusician($connection,$mId,$rating,$ratedBy){
    
            if($result=mysqli_query($connection,"SELECT COUNT(*) AS numRates FROM musiciansrating WHERE ratedBy='$ratedBy' AND mId='$mId' ")){
                $result=mysqli_fetch_assoc($result);
                if($result['numRates']>0){
                    //duplicates rating
                    //update the ratings
                    if(mysqli_query($connection,"UPDATE musiciansrating SET mScore='$rating' WHERE mId='$mId' AND ratedBy='$ratedBy' ")){
                        return true;
                    }else{
                        return false;                    
                    }
                }else{
    
                    if(mysqli_query($connection,"INSERT INTO `musiciansrating` (mId,mScore,ratedBy) VALUES('$mId','$rating','$ratedBy') ")){
                        return true;
                    }else{
                        return false;
                    }
    
                }
    
    
            }else{
                return false;
            }
    
        
    
        }
    
        function followMusician($connection,$followedBy,$followedTo){
            //insert into mFollowers then update the musicians followers
            
            if($result=mysqli_query($connection,"SELECT COUNT(*) AS totalFollowers FROM mfollowers WHERE followedTo='$followedTo' AND followedBy='$followedBy' ")){//avoid duplicates
                $result=mysqli_fetch_assoc($result);
                $totalFollowers=$result['totalFollowers'];
                if($totalFollowers>0){
                    return false;//already followed by him
                }else{
                    //insert new data
                if(mysqli_query($connection,"INSERT INTO mfollowers(followedTo,followedBy) VALUES('$followedTo','$followedBy') ")){
                    $result=mysqli_query($connection,"SELECT COUNT(*) AS totalFollowers FROM mfollowers WHERE followedTo='$followedTo' ");
                    $result=mysqli_fetch_assoc($result);
                    $totalFollowers=$result['totalFollowers'];
                
                // echo $totalFollowers;
                if(mysqli_query($connection,"UPDATE musicians SET mFollowers='$totalFollowers' WHERE mId='$followedTo' ")){
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
       
            }
    
        }else{
            return false;
        }
    }
    

    function getMusiciansRecommendationByLocation($connection,$mCredentials){
        
        $tempRecommendedMusiciansList=[];//without distance value
        $recommendedMusiciansList=[];//with distance value
            
        $lat1=$mCredentials['mLocationLat'];
        $lon1=$mCredentials['mLocationLon'];
        $mId=$mCredentials['mId'];//dont recommend me
        
        
        

        $musiciansQ=mysqli_query($connection,
        "SELECT M.mId,M.mName,M.mFollowers,M.mType,M.mLocationLat,M.mLocationLon,M.mProfileP,M.mScore,B.bName 
        FROM musicians as M INNER JOIN bands as B ON M.bId= B.bId WHERE M.mId!='$mId' ");// M.mId!='$mId' : dont recommend myself

        while($each=mysqli_fetch_assoc($musiciansQ)){

            $eachMusician=new Musicians;
            $eachMusician->mLocationLat=$each['mLocationLat'];
            $eachMusician->mLocationLon=$each['mLocationLon'];
            $eachMusician->mName=$each['mName'];
            $eachMusician->mType=$each['mType'];
            $eachMusician->mScore=$each['mScore'];
            $eachMusician->mProfileP=$each['mProfileP'];
            $eachMusician->mId=$each['mId'];
            $eachMusician->mFollowers=$each['mFollowers'];
            
            $eachMusician->bName=$each['bName'];
            
            $tempRecommendedMusiciansList[]=$eachMusician;
            
            // echo 'bands are'.$each['bName'].'</br>';
            
        }

        //find the nearby bands
        foreach($tempRecommendedMusiciansList as $each){
        
            $lon2=$each->mLocationLon;
            $lat2=$each->mLocationLat;
            $theta = $lon1 - $lon2;//lon2
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;                
            $each->distance=$miles;//update the distance of that bandObject
            $recommendedMusiciansList[]=$each;
        }
        //filter the recommendedBandList and get the top 10 bands nearby you
        //-1 (smaller than), 0 (equal to), or 1 (larger than)
        usort($recommendedMusiciansList,"customLocationSortingByLocation");
        //recommendedBandList has been sorted

        echo json_encode($recommendedMusiciansList);
        
    }


    function rateMusicians($connection,$mId,$rating,$ratedBy){

        if($result=mysqli_query($connection,"SELECT COUNT(*) AS numRates FROM musiciansrating WHERE ratedBy='$ratedBy' AND mId='$mId' ")){
            $result=mysqli_fetch_assoc($result);
            if($result['numRates']>0){
                //duplicates rating
                //update the ratings
                if(mysqli_query($connection,"UPDATE musiciansrating SET mScore='$rating' WHERE mId='$mId' AND ratedBy='$ratedBy' ")){
                    return true;
                }else{
                    return false;                    
                }
            }else{

                if(mysqli_query($connection,"INSERT INTO `musiciansrating` (mId,mScore,ratedBy) VALUES('$mId','$rating','$ratedBy') ")){
                    return true;
                }else{
                    return false;
                }

                }
            }else{
                return false;
            }


    }

    function followMusicians($connection,$followedBy,$followedTo){
        //insert into mFollowers then update the musicians followers
        
        if($result=mysqli_query($connection,"SELECT COUNT(*) AS totalFollowers FROM mfollowers WHERE followedTo='$followedTo' AND followedBy='$followedBy' ")){//avoid duplicates
            $result=mysqli_fetch_assoc($result);
            $totalFollowers=$result['totalFollowers'];
            if($totalFollowers>0){
                return false;//already followed by him
            }else{
                //insert new data
            if(mysqli_query($connection,"INSERT INTO mfollowers(followedTo,followedBy) VALUES('$followedTo','$followedBy') ")){
                $result=mysqli_query($connection,"SELECT COUNT(*) AS totalFollowers FROM mfollowers WHERE followedTo='$followedTo' ");
                $result=mysqli_fetch_assoc($result);
                $totalFollowers=$result['totalFollowers'];
            
            // echo $totalFollowers;
            if(mysqli_query($connection,"UPDATE musicians SET mFollowers='$totalFollowers' WHERE mId='$followedTo' ")){
                return true;
            }else{
                return false;
            }
            }else{
                return false;
            }

            }

        }else{
            return false;
        }
    }

}


//get the musicians on the basis of the type
if(isset($_POST['getRecommendedMusicians']) && isset($_POST['filterType'])){
    $connection=new Connection('localhost','root','','bandsville');
    $connection=$connection->connect();
    
    $mCredentials=json_decode($_POST['getRecommendedMusicians'],true);
    $mObject=new Musicians;

    if($_POST['filterType']=='location'){
        $mObject->getMusiciansRecommendationByLocation($connection,$mCredentials);//nearby best scores of all type
    }else{
        $mObject->getMusiciansRecommendationByScore($connection,$mCredentials);//best scores of all type
    }

}
//


//rate musicians
if(isset($_POST['rateMusicians']) && isset($_POST['ratedScore']) && isset($_POST['mId']) && isset($_POST['ratedBy'])){
    $connection=new Connection('localhost','root','','bandsville');
    $connection=$connection->connect();

    $mId=$_POST['mId'];
    $ratedScore=$_POST['ratedScore'];
    $ratedBy=$_POST['ratedBy'];

    $statusObject=new Status;
    $mObject=new Musicians;
    if($mObject->rateMusicians($connection,$mId,$ratedScore,$ratedBy)){
        $statusObject->statusCode='success';
        $statusObject->statusType='rating';
        echo json_encode($statusObject);
    }else{
        $statusObject->statusCode='failed';
        $statusObject->statusType='rating';
        echo json_encode($statusObject);
    }
    

}

if(isset($_POST['followMusicians']) && isset($_POST['followedBy']) && isset($_POST['followedTo'])){
    $connection=new Connection('localhost','root','','bandsville');
    $connection=$connection->connect();

    $followedBy=$_POST['followedBy'];//mid
    $followedTo=$_POST['followedTo'];//mid
    
    $statusObject=new Status;
    $mObject=new Musicians;
    if($mObject->followMusicians($connection,$followedBy,$followedTo)){
        $statusObject->statusCode='success';
        $statusObject->statusType='following';
        echo json_encode($statusObject);
    }else{
        $statusObject->statusCode='failed';
        $statusObject->statusType='following';
        echo json_encode($statusObject);
    }
    

}


if(isset($_POST['syncUserPhotos']) && isset($_POST['mProfileP']) && isset($_POST['mId'])){
    $connection=new Connection('localhost','root','','bandsville');
    $connection=$connection->connect();
    $mObject=new Musicians;
    $statusObject=new Status;
    if($mObject->syncUserPhoto($connection,$_POST['mId'],$_POST['mProfileP'])){
        
        $statusObject->statusCode='success';
        $statusObject->statusType='syncUserPhotos';
        echo json_encode($statusObject);
    }else{
        $statusObject->statusCode='failed';
        $statusObject->statusType='syncUserPhotos';
        echo json_encode($statusObject);

    }
   

}


//This is the recommendation alogorithm for the musicians by weighted formula
//this is run by the admin 
function updateNewMusicianScore($connection){
    
    $result=mysqli_query($connection,
    "SELECT M.mFollowers,M.mId,AVG(R.mScore) as mRating FROM musicians as M INNER JOIN musiciansrating as R ON R.mId = M.mId GROUP BY mId");
    // "SELECT *,AVG(newResult.bRating) AS C FROM ( SELECT B.bFans,B.bId,AVG(R.bScore) as bRating FROM bands as B INNER JOIN bandsrating as R ON R.bId = B.bId GROUP BY bId)newResult";
    
    $musiciansList=NULL;
    $followersList=NULL;
    
    if($result){
        while($each=mysqli_fetch_assoc($result)){
            $mObject=new Musicians;
            $mObject->mId=$each['mId'];
            $mObject->mRating=$each['mRating'];//R
            $mObject->mFollowers=$each['mFollowers'];//V in case of band it is bFans
            $followersList[]=$each['mFollowers'];
            $musiciansList[]=$mObject;
            //send json as {bId,bRating}
        }

        if($musiciansList!=NULL){
            //mean rating of all musicians/bands as C
            $CQuery="SELECT AVG(newResult.mRating) as C FROM 
            ( SELECT M.mFollowers,M.mId,AVG(R.mScore) as mRating FROM musicians as M INNER JOIN musiciansrating as R ON R.mId = M.mId GROUP BY M.mId)newResult";
            $result=mysqli_query($connection,$CQuery);
            $result=mysqli_fetch_assoc($result);
            $C=$result['C'];

            //create array of followers and sent to percentile formula to get the percentile score as M
           $M= get_percentile(75,$followersList);
           echo json_encode($followersList);

           foreach($musiciansList as $each){
            $mId=$each->mId;
            $V=$each->mFollowers;
            $R=$each->mRating;
            $weightedScore=( (($V*$R)/($V+$M)) +(($M*$C)/($M+$V)) );
            echo 'new score of '.$each->bName.''.' is '.$weightedScore."</br>"; 
            //update the row
            mysqli_query($connection,"UPDATE musicians SET mScore='$weightedScore' WHERE mId='$mId' ");
        }
         
            
            
        }else{
            $musiciansList=[];
            echo json_encode($musiciansList);

        }

     

    }

}
/*Uncomment below 3 lines to start the above updateNewMusicianScore method*/

// $connection=new Connection('localhost','root','','bandsville');
// $connection=$connection->connect();
// updateNewMusicianScore($connection);










?>
